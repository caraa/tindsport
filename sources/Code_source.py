from tkinter import *
from PIL import Image, ImageTk



def opennewWindow():
    """Affichage de la première page du questionnaire et où apparait un bouton permettant de quitter la fenêtre ouverte; sujet : catégorie d'âge.
    """
    # Initialisation des variables de contage globales
    global categorie_collectif
    categorie_collectif = 0
    global categorie_raquette
    categorie_raquette = 0
    global categorie_plaisant
    categorie_plaisant = 0
    global categorie_combat
    categorie_combat = 0
    global categorie_eau
    categorie_eau = 0
    global categorie_special
    categorie_special = 0
    global newWindow
    # création de la première fenêtre du questionnaire en définnissant les caractéristiques de la pâge (couleur de fond / teste)
    newWindow = Toplevel(Mafenetre)
    newWindow.title("question 1")
    newWindow.attributes('-fullscreen', True)
    newWindow['bg']='#608892'
    # création d'un bouton qui permet de quitter/détruire la fenêtre
    Bouton_quit = Button(newWindow, text = 'Quitter', command = newWindow.destroy, bg = 'red', activebackground = 'black')
    Bouton_quit.place(x=700,y=430)

    # création de radiobouton et d'une question avec un label tout en reglant la place de ces éléments
    label = Label(newWindow, text="Dans quelles catégories d'âge êtes vous ?")
    radiobutton = Radiobutton(newWindow,text='Junior',command = question_1_j,value = 1)
    radiobutton2 = Radiobutton(newWindow,text='Adolescent', command = question_1_a_m, value = 2)
    radiobutton3 = Radiobutton(newWindow,text='Majeur', command = question_1_a_m, value = 3)
    radiobutton4 = Radiobutton(newWindow,text="Sénior", command = question_1_s, value = 4)


    label.place(x=675,y=300)
    radiobutton.place(x=700,y=320)
    radiobutton2.place(x=700,y=340)
    radiobutton3.place(x=700,y=360)
    radiobutton4.place(x=700,y=380)


def question_1_j():
    """Affecte aux variable de contage global 1 pour déterminer la catégorie la
    mieux adaptée, et ferme la fenêtre précèdente en ouvrant la suivante. La fonction
    s'active aux choix de la question de la fenêtre précèdente
    """
    # Appel de la fonction ouvrant la fenêtre suivante et destruction de la précèdente
    global newWindow
    opennewWindow2()
    newWindow.destroy()
    # Affectation de 1 au variables de contages globales
    global categorie_collectif
    categorie_collectif = categorie_collectif+1
    global categorie_raquette
    categorie_raquette = categorie_raquette+1
    global categorie_plaisant
    categorie_plaisant = categorie_plaisant+1
    global categorie_eau
    categorie_eau = categorie_eau+1
    global categorie_special
    categorie_special = categorie_special+1

def question_1_a_m():
    """Affecte aux variable de contage global 1 pour déterminer la catégorie la
    mieux adaptée, et ferme la fenêtre précèdente en ouvrant la suivante. La fonction
    s'active aux choix de la question de la fenêtre précèdente
    """
    # Appel de la fonction ouvrant la fenêtre suivante et destruction de la précèdente
    global newWindow
    opennewWindow2()
    newWindow.destroy()
    # Affectation de 1 au variables de contages globales
    global categorie_collectif
    categorie_collectif = categorie_collectif+1
    global categorie_raquette
    categorie_raquette = categorie_raquette+1
    global categorie_plaisant
    categorie_plaisant = categorie_plaisant+1
    global categorie_combat
    categorie_combat = categorie_combat+1
    global categorie_eau
    categorie_eau = categorie_eau+1
    global categorie_special
    categorie_special = categorie_special+1

def question_1_s():
    """Affecte aux variable de contage global 1 pour déterminer la catégorie la
    mieux adaptée, et ferme la fenêtre précèdente en ouvrant la suivante. La fonction
    s'active aux choix de la question de la fenêtre précèdente
    """
    # Appel de la fonction ouvrant la fenêtre suivante et destruction de la précèdente
    global newWindow
    opennewWindow2()
    newWindow.destroy()
    # Affectation de 1 au variables de contages globales
    global categorie_plaisant
    categorie_plaisant = categorie_plaisant+1
    global categorie_eau
    categorie_eau = categorie_eau+1



def opennewWindow2():
    """Affichage de la seconde page du questionnaire et où apparait un bouton permettant de quitter la fenêtre ouverte; sujet : intensité de pratique de sport.
    """
    global newWindow2
    # création de la deuxième fenêtre du questionnaire en définnissant les caractéristiques de la pâge (couleur de fond / teste)
    newWindow2 = Toplevel(Mafenetre)
    newWindow2.title("question 2")
    newWindow2.attributes('-fullscreen', True)
    newWindow2['bg']='#608892'
    # création d'un bouton qui permet de quitter/détruire la fenêtre
    Bouton_quit2 = Button(newWindow2, text = 'Quitter', command = newWindow2.destroy, bg = 'red', activebackground = 'black')
    Bouton_quit2.place(x=700,y=410)

    # création de radiobouton et d'une question avec un label tout en reglant la place de ces éléments
    label = Label(newWindow2, text="À quelle intensité faites vous des activitées physiques ?")
    radiobutton5 = Radiobutton(newWindow2,text='Faible', command = question_2_f, value = 5)
    radiobutton6 = Radiobutton(newWindow2,text='Modéré', command = question_2_m_i, value = 6)
    radiobutton7 = Radiobutton(newWindow2,text='Intense', command = question_2_m_i, value = 7)

    label.place(x=675,y=300)
    radiobutton5.place(x=700,y=320)
    radiobutton6.place(x=700,y=340)
    radiobutton7.place(x=700,y=360)


def question_2_f():
    """Affecte aux variable de contage global 1 pour déterminer la catégorie la
    mieux adaptée, et ferme la fenêtre précèdente en ouvrant la suivante. La fonction
    s'active aux choix de la question de la fenêtre précèdente
    """
    # Appel de la fonction ouvrant la fenêtre suivante et destruction de la précèdente
    global newWindow2
    opennewWindow3()
    newWindow2.destroy()
    # Affectation de 1 au variables de contages globales
    global categorie_raquette
    categorie_raquette = categorie_raquette+1
    global categorie_plaisant
    categorie_plaisant = categorie_plaisant+1
    global categorie_eau
    categorie_eau = categorie_eau+1
    global categorie_special
    categorie_special = categorie_special+1

def question_2_m_i():
    """Affecte aux variable de contage global 1 pour déterminer la catégorie la
    mieux adaptée, et ferme la fenêtre précèdente en ouvrant la suivante. La fonction
    s'active aux choix de la question de la fenêtre précèdente
    """
    # Appel de la fonction ouvrant la fenêtre suivante et destruction de la précèdente
    global newWindow2
    opennewWindow3()
    newWindow2.destroy()
    # Affectation de 1 au variables de contages globales
    global categorie_collectif
    categorie_collectif = categorie_collectif+1
    global categorie_raquette
    categorie_raquette = categorie_raquette+1
    global categorie_plaisant
    categorie_plaisant = categorie_plaisant+1
    global categorie_combat
    categorie_combat = categorie_combat+1
    global categorie_eau
    categorie_eau = categorie_eau+1
    global categorie_special
    categorie_special = categorie_special+1


def opennewWindow3():
    """Affichage de la troisième page du questionnaire et où apparait un bouton permettant de quitter la fenêtre ouverte; sujet : environnemnt de pratique du sport.
    """
    global newWindow3
    # création de la troisième fenêtre du questionnaire en définnissant les caractéristiques de la pâge (couleur de fond / teste)
    newWindow3 = Toplevel(Mafenetre)
    newWindow3.title("question 3")
    newWindow3.attributes('-fullscreen', True)
    newWindow3['bg']='#608892'
    # création d'un bouton qui permet de quitter/détruire la fenêtre
    Bouton_quit3 = Button(newWindow3, text = 'Quitter', command = newWindow3.destroy, bg = 'red', activebackground = 'black')
    Bouton_quit3.place(x=700,y=390)

    # création de radiobouton et d'une question avec un label tout en reglant la place de ces éléments
    label = Label(newWindow3, text="Dans quel envirronement aimez vous faire du sport ?")
    radiobutton8 = Radiobutton(newWindow3,text="En intérieur", command =question_3_i, value = 8)
    radiobutton9 = Radiobutton(newWindow3,text="En extérieur", command =question_3_e, value = 9)

    label.place(x=675,y=300)
    radiobutton8.place(x=700,y=320)
    radiobutton9.place(x=700,y=340)


def question_3_i():
    """Affecte aux variable de contage global 1 pour déterminer la catégorie la
    mieux adaptée, et ferme la fenêtre précèdente en ouvrant la suivante. La fonction
    s'active aux choix de la question de la fenêtre précèdente
    """
    # Appel de la fonction ouvrant la fenêtre suivante et destruction de la précèdente
    global newWindow3
    opennewWindow4()
    newWindow3.destroy()
    # Affectation de 1 au variables de contages globales
    global categorie_collectif
    categorie_collectif =categorie_collectif+1
    global categorie_raquette
    categorie_raquette = categorie_raquette+1
    global categorie_combat
    categorie_combat = categorie_combat+1
    global categorie_eau
    categorie_eau = categorie_eau+1

def question_3_e():
    """Affecte aux variable de contage global 1 pour déterminer la catégorie la
    mieux adaptée, et ferme la fenêtre précèdente en ouvrant la suivante. La fonction
    s'active aux choix de la question de la fenêtre précèdente
    """
    # Appel de la fonction ouvrant la fenêtre suivante et destruction de la précèdente
    global newWindow3
    opennewWindow4()
    newWindow3.destroy()
    # Affectation de 1 au variables de contages globales
    global categorie_collectif
    categorie_collectif = categorie_collectif+1
    global categorie_raquette
    categorie_raquette = categorie_raquette+1
    global categorie_plaisant
    categorie_plaisant = categorie_plaisant+1
    global categorie_eau
    categorie_eau = categorie_eau+1
    global categorie_special
    categorie_special = categorie_special+1


def opennewWindow4():
    """Affichage de la quatrième page du questionnaire et où apparait un bouton permettant de quitter la fenêtre ouverte; sujet : combien de personnes avec qui pratiquer le sport.
    """
    global newWindow4
    # création de la quatrième fenêtre du questionnaire en définnissant les caractéristiques de la pâge (couleur de fond / teste)
    newWindow4 = Toplevel(Mafenetre)
    newWindow4.title("question 4")
    newWindow4.attributes('-fullscreen', True)
    newWindow4['bg']='#608892'
    # création d'un bouton qui permet de quitter/détruire la fenêtre
    Bouton_quit4 = Button(newWindow4, text = 'Quitter', command = newWindow4.destroy, bg = 'red', activebackground = 'black')
    Bouton_quit4.place(x=700,y=390)

    # création de radiobouton et d'une question avec un label tout en reglant la place de ces éléments
    label = Label(newWindow4, text="Vous préférez faire du sport ?")
    radiobutton10 = Radiobutton(newWindow4,text="En colléctif", command =question_4_c, value = 10)
    radiobutton11 = Radiobutton(newWindow4,text="En individuel", command =question_4_i, value = 11)

    label.place(x=675,y=300)
    radiobutton10.place(x=700,y=320)
    radiobutton11.place(x=700,y=340)


def question_4_c():
    """Affecte aux variable de contage global 1 pour déterminer la catégorie la
    mieux adaptée, et ferme la fenêtre précèdente en ouvrant la suivante. La fonction
    s'active aux choix de la question de la fenêtre précèdente
    """
    # Appel de la fonction ouvrant la fenêtre suivante et destruction de la précèdente
    global newWindow4
    opennewWindow5()
    newWindow4.destroy()
    # Affectation de 1 au variables de contages globales
    global categorie_collectif
    categorie_collectif = categorie_collectif+1
    global categorie_plaisant
    categorie_plaisant = categorie_plaisant+1
    global categorie_eau
    categorie_eau = categorie_eau+1

def question_4_i():
    """Affecte aux variable de contage global 1 pour déterminer la catégorie la
    mieux adaptée, et ferme la fenêtre précèdente en ouvrant la suivante. La fonction
    s'active aux choix de la question de la fenêtre précèdente
    """
    # Appel de la fonction ouvrant la fenêtre suivante et destruction de la précèdente
    global newWindow4
    opennewWindow5()
    newWindow4.destroy()
    # Affectation de 1 au variables de contages globales
    global categorie_raquette
    categorie_raquette = categorie_raquette+1
    global categorie_plaisant
    categorie_plaisant = categorie_plaisant+1
    global categorie_combat
    categorie_combat = categorie_combat+1
    global categorie_eau
    categorie_eau = categorie_eau+1
    global categorie_special
    categorie_special = categorie_special+1


def opennewWindow5():
    """Affichage de la cinquième page du questionnaire et où apparait un bouton permettant de quitter la fenêtre ouverte; sujet : pourquoi faire du sport.
    """
    global newWindow5
    # création de la cinquième fenêtre du questionnaire en définnissant les caractéristiques de la pâge (couleur de fond / teste)
    newWindow5 = Toplevel(Mafenetre)
    newWindow5.title("question 5")
    newWindow5.attributes('-fullscreen', True)
    newWindow5['bg']='#608892'
    # création d'un bouton qui permet de quitter/détruire la fenêtre
    Bouton_quit5 = Button(newWindow5, text = 'Quitter', command = newWindow5.destroy, bg = 'red', activebackground = 'black')
    Bouton_quit5.place(x=700,y=390)

    # création de radiobouton et d'une question avec un label tout en reglant la place de ces éléments
    label = Label(newWindow5, text="Pour vous le sport c'est principalement quoi ?")
    radiobutton12 = Radiobutton(newWindow5,text="Rester en forme", command =question_5_ref_gll, value = 12)
    radiobutton13 = Radiobutton(newWindow5,text="Se détendre", command =question_5_sd, value = 13)
    radiobutton14 = Radiobutton(newWindow5,text="Garder la ligne", command =question_5_ref_gll, value = 14)
    radiobutton15 = Radiobutton(newWindow5,text="Rencontrer des personnes", command =question_5_rdp, value = 15)
    radiobutton16 = Radiobutton(newWindow5,text="S'amuser", command =question_5_sa, value = 16)
    radiobutton17 = Radiobutton(newWindow5,text="Faire du sport en famille", command =question_5_rdp, value = 17)


    label.place(x=675,y=300)
    radiobutton12.place(x=700,y=320)
    radiobutton13.place(x=700,y=340)
    radiobutton14.place(x=700,y=360)
    radiobutton15.place(x=700,y=380)
    radiobutton16.place(x=700,y=400)
    radiobutton17.place(x=700,y=420)


def question_5_ref_gll():
    """Affecte aux variable de contage global 1 pour déterminer la catégorie la
    mieux adaptée, et ferme la fenêtre précèdente en ouvrant la suivante. La fonction
    s'active aux choix de la question de la fenêtre précèdente
    """
    # Appel de la fonction ouvrant la fenêtre suivante et destruction de la précèdente
    global newWindow5
    opennewWindow6()
    newWindow5.destroy()
    # Affectation de 1 au variables de contages globales
    global categorie_collectif
    categorie_collectif = categorie_collectif +1
    global categorie_raquette
    categorie_raquette = categorie_raquette+1
    global categorie_plaisant
    categorie_plaisant = categorie_plaisant+1
    global categorie_eau
    categorie_eau = categorie_eau+1
    global categorie_special
    categorie_special = categorie_special+1

def question_5_sd():
    """Affecte aux variable de contage global 1 pour déterminer la catégorie la
    mieux adaptée, et ferme la fenêtre précèdente en ouvrant la suivante. La fonction
    s'active aux choix de la question de la fenêtre précèdente
    """
    # Appel de la fonction ouvrant la fenêtre suivante et destruction de la précèdente
    global newWindow5
    opennewWindow6()
    newWindow5.destroy()
    # Affectation de 1 au variables de contages globales
    global categorie_plaisant
    categorie_plaisant = categorie_plaisant+1

def question_5_rdp():
    """Affecte aux variable de contage global 1 pour déterminer la catégorie la
    mieux adaptée, et ferme la fenêtre précèdente en ouvrant la suivante. La fonction
    s'active aux choix de la question de la fenêtre précèdente
    """
    # Appel de la fonction ouvrant la fenêtre suivante et destruction de la précèdente
    global newWindow5
    opennewWindow6()
    newWindow5.destroy()
    # Affectation de 1 au variables de contages globales
    global categorie_collectif
    categorie_collectif = categorie_collectif +1

def question_5_sa():
    """Affecte aux variable de contage global 1 pour déterminer la catégorie la
    mieux adaptée, et ferme la fenêtre précèdente en ouvrant la suivante. La fonction
    s'active aux choix de la question de la fenêtre précèdente
    """
    # Appel de la fonction ouvrant la fenêtre suivante et destruction de la précèdente
    global newWindow5
    opennewWindow6()
    newWindow5.destroy()
    # Affectation de 1 au variables de contages globales
    global categorie_collectif
    categorie_collectif = categorie_collectif +1
    global categorie_raquette
    categorie_raquette = categorie_raquette+1
    global categorie_plaisant
    categorie_plaisant = categorie_plaisant+1
    global categorie_eau
    categorie_eau = categorie_eau+1


def opennewWindow6():
    """Affichage de la sixième page du questionnaire et où apparait un bouton permettant de quitter la fenêtre ouverte; sujet : pourquoi faire du sport.
    """
    global newWindow6
    # création de la sixième fenêtre du questionnaire en définnissant les caractéristiques de la pâge (couleur de fond / teste)
    newWindow6 = Toplevel(Mafenetre)
    newWindow6.title("question 6")
    newWindow6.attributes('-fullscreen', True)
    newWindow6['bg']='#608892'
    # création d'un bouton qui permet de quitter/détruire la fenêtre
    Bouton_quit6 = Button(newWindow6, text = 'Quitter', command = newWindow6.destroy, bg = 'red', activebackground = 'black')
    Bouton_quit6.place(x=700,y=390)

    # création de radiobouton et d'une question avec un label tout en reglant la place de ces éléments
    label = Label(newWindow6, text="Dans quel but voulez vous faire du sport ?")
    radiobutton18 = Radiobutton(newWindow6,text="Faire du sport sans se ruiner", command =question_6_ssr, value = 18)
    radiobutton19 = Radiobutton(newWindow6,text="Faire du sport à son rythme", command =question_6_asr, value = 19)
    radiobutton20 = Radiobutton(newWindow6,text="Être accompagné", command =question_6_ea, value = 20)
    radiobutton21 = Radiobutton(newWindow6,text="Près de chez soi", command =question_6_ssr, value = 21)
    radiobutton22 = Radiobutton(newWindow6,text="Découvrir de nouvelle activitées", command =question_6_new, value = 22)
    radiobutton23 = Radiobutton(newWindow6,text="Se muscler", command =question_6_sm, value = 23)

    label.place(x=675,y=300)
    radiobutton18.place(x=700,y=320)
    radiobutton19.place(x=700,y=340)
    radiobutton20.place(x=700,y=360)
    radiobutton21.place(x=700,y=380)
    radiobutton22.place(x=700,y=400)
    radiobutton23.place(x=700,y=420)


def question_6_ssr():
    """Affecte aux variable de contage global 1 pour déterminer la catégorie la
    mieux adaptée, et ferme la fenêtre précèdente en ouvrant la suivante. La fonction
    s'active aux choix de la question de la fenêtre précèdente
    """
    # Appel de la fonction ouvrant la fenêtre suivante et destruction de la précèdente
    global newWindow6
    opennewWindow7()
    newWindow6.destroy()
    # Affectation de 1 au variables de contages globales
    global categorie_collectif
    categorie_collectif =categorie_collectif+1
    global categorie_raquette
    categorie_raquette = categorie_raquette+1
    global categorie_plaisir
    categorie_plaisir = categorie_plaisir+1
    global categorie_eau
    categorie_eau = categorie_eau+1

def question_6_asr():
    """Affecte aux variable de contage global 1 pour déterminer la catégorie la
    mieux adaptée, et ferme la fenêtre précèdente en ouvrant la suivante. La fonction
    s'active aux choix de la question de la fenêtre précèdente
    """
    # Appel de la fonction ouvrant la fenêtre suivante et destruction de la précèdente
    global newWindow6
    opennewWindow7()
    newWindow6.destroy()
    # Affectation de 1 au variables de contages globales
    global categorie_collectif
    categorie_collectif = categorie_collectif+1
    global categorie_raquette
    categorie_raquette = categorie_raquette+1
    global categorie_plaisant
    categorie_plaisant = categorie_plaisant+1
    global categorie_eau
    categorie_eau = categorie_eau+1
    global categorie_special
    categorie_special = categorie_special+1

def question_6_ea():
    """Affecte aux variable de contage global 1 pour déterminer la catégorie la
    mieux adaptée, et ferme la fenêtre précèdente en ouvrant la suivante. La fonction
    s'active aux choix de la question de la fenêtre précèdente
    """
    # Appel de la fonction ouvrant la fenêtre suivante et destruction de la précèdente
    global newWindow6
    opennewWindow7()
    newWindow6.destroy()
    # Affectation de 1 au variables de contages globales
    global categorie_collectif
    categorie_collectif =categorie_collectif+1
    global categorie_plaisir
    categorie_plaisir = categorie_plaisir+1
    global categorie_eau
    categorie_eau = categorie_eau+1

def question_6_new():
    """Affecte aux variable de contage global 1 pour déterminer la catégorie la
    mieux adaptée, et ferme la fenêtre précèdente en ouvrant la suivante. La fonction
    s'active aux choix de la question de la fenêtre précèdente
    """
    # Appel de la fonction ouvrant la fenêtre suivante et destruction de la précèdente
    global newWindow6
    opennewWindow7()
    newWindow6.destroy()
    # Affectation de 1 au variables de contages globales
    global categorie_plaisant
    categorie_plaisant = categorie_plaisant+1
    global categorie_combat
    categorie_combat = categorie_combat+1
    global categorie_special
    categorie_special = categorie_special+1

def question_6_sm():
    """Affecte aux variable de contage global 1 pour déterminer la catégorie la
    mieux adaptée, et ferme la fenêtre précèdente en ouvrant la suivante. La fonction
    s'active aux choix de la question de la fenêtre précèdente
    """
    # Appel de la fonction ouvrant la fenêtre suivante et destruction de la précèdente
    global newWindow6
    opennewWindow7()
    newWindow6.destroy()
    # Affectation de 1 au variables de contages globales
    global categorie_collectif
    categorie_collectif = categorie_collectif+1
    global categorie_special
    categorie_special = categorie_special+1


def opennewWindow7():
    """Affichage de la septième page du questionnaire et où apparait un bouton permettant de quitter la fenêtre ouverte; sujet : qualitées.
    """
    global newWindow7
    # création de la septième fenêtre du questionnaire en définnissant les caractéristiques de la pâge (couleur de fond / teste)
    newWindow7 = Toplevel(Mafenetre)
    newWindow7.title("question 7")
    newWindow7.attributes('-fullscreen', True)
    newWindow7['bg']='#608892'
    # création d'un bouton qui permet de quitter/détruire la fenêtre
    Bouton_quit7 = Button(newWindow7, text = 'Quitter', command = newWindow7.destroy, bg = 'red', activebackground = 'black')
    Bouton_quit7.place(x=700,y=390)

    # création de radiobouton et d'une question avec un label tout en reglant la place de ces éléments
    label = Label(newWindow7, text="Quel est votre qualité principale ?")
    radiobutton24 = Radiobutton(newWindow7,text="Précision", command =question_7_p, value = 14)
    radiobutton25 = Radiobutton(newWindow7,text="Vitesse", command =question_7_v, value = 25)
    radiobutton26 = Radiobutton(newWindow7,text="Force", command =question_7_f, value = 26)
    radiobutton27 = Radiobutton(newWindow7,text="Endurance", command =question_7_e, value = 27)
    radiobutton28 = Radiobutton(newWindow7,text="Agilité", command =question_7_a, value = 28)
    radiobutton29 = Radiobutton(newWindow7,text="Souplesse", command =question_7_p, value = 29)


    label.place(x=675,y=300)
    radiobutton24.place(x=700,y=320)
    radiobutton25.place(x=700,y=340)
    radiobutton26.place(x=700,y=360)
    radiobutton27.place(x=700,y=380)
    radiobutton28.place(x=700,y=400)
    radiobutton29.place(x=700,y=420)


def question_7_p():
    """Affecte aux variable de contage global 1 pour déterminer la catégorie la
    mieux adaptée, et ferme la fenêtre précèdente en ouvrant la suivante. La fonction
    s'active aux choix de la question de la fenêtre précèdente
    """
    # Appel de la fonction ouvrant la fenêtre suivante et destruction de la précèdente
    global newWindow7
    opennewWindow8()
    newWindow7.destroy()
    # Affectation de 1 au variables de contages globales
    global categorie_combat
    categorie_combat = categorie_combat+1

def question_7_v():
    """Affecte aux variable de contage global 1 pour déterminer la catégorie la
    mieux adaptée, et ferme la fenêtre précèdente en ouvrant la suivante. La fonction
    s'active aux choix de la question de la fenêtre précèdente
    """
    # Appel de la fonction ouvrant la fenêtre suivante et destruction de la précèdente
    global newWindow7
    opennewWindow8()
    newWindow7.destroy()
    # Affectation de 1 au variables de contages globales
    global categorie_collectif
    categorie_collectif = categorie_collectif+1
    global categorie_raquette
    categorie_raquette = categorie_raquette+1
    global categorie_combat
    categorie_combat = categorie_combat+1

def question_7_f():
    """Affecte aux variable de contage global 1 pour déterminer la catégorie la
    mieux adaptée, et ferme la fenêtre précèdente en ouvrant la suivante. La fonction
    s'active aux choix de la question de la fenêtre précèdente
    """
    # Appel de la fonction ouvrant la fenêtre suivante et destruction de la précèdente
    global newWindow7
    opennewWindow8()
    newWindow7.destroy()
    # Affectation de 1 au variables de contages globales
    global categorie_collectif
    categorie_collectif = categorie_collectif+1
    global categorie_raquette
    categorie_raquette = categorie_raquette+1
    global categorie_special
    categorie_special = categorie_special+1

def question_7_e():
    """Affecte aux variable de contage global 1 pour déterminer la catégorie la
    mieux adaptée, et ferme la fenêtre précèdente en ouvrant la suivante. La fonction
    s'active aux choix de la question de la fenêtre précèdente
    """
    # Appel de la fonction ouvrant la fenêtre suivante et destruction de la précèdente
    global newWindow7
    opennewWindow8()
    newWindow7.destroy()
    # Affectation de 1 au variables de contages globales
    global categorie_collectif
    categorie_collectif = categorie_collectif+1
    global categorie_plaisant
    categorie_plaisant = categorie_plaisant+1

def question_7_a():
    """Affecte aux variable de contage global 1 pour déterminer la catégorie la
    mieux adaptée, et ferme la fenêtre précèdente en ouvrant la suivante. La fonction
    s'active aux choix de la question de la fenêtre précèdente
    """
    # Appel de la fonction ouvrant la fenêtre suivante et destruction de la précèdente
    global newWindow7
    opennewWindow8()
    newWindow7.destroy()
    # Affectation de 1 au variables de contages globales
    global categorie_combat
    categorie_combat = categorie_combat+1
    global categorie_special
    categorie_special = categorie_special+1


def opennewWindow8():
    """Affichage de la huitième page du questionnaire et où apparait un bouton permettant de quitter la fenêtre ouverte; sujet : handicap.
    """
    global newWindow8
    # création de la huitième fenêtre du questionnaire en définnissant les caractéristiques de la pâge (couleur de fond / teste)
    newWindow8 = Toplevel(Mafenetre)
    newWindow8.title("question 8")
    newWindow8.attributes('-fullscreen', True)
    newWindow8['bg']='#608892'
    # création d'un bouton qui permet de quitter/détruire la fenêtre
    Bouton_quit8 = Button(newWindow8, text = 'Quitter', command = newWindow8.destroy, bg = 'red', activebackground = 'black')
    Bouton_quit8.place(x=700,y=390)

    # création de radiobouton et d'une question avec un label tout en reglant la place de ces éléments
    label = Label(newWindow8, text="Êtes vous en situation de handicap ?")
    radiobutton30 = Radiobutton(newWindow8,text="Oui", command =question_8_o, value = 30)
    radiobutton31 = Radiobutton(newWindow8,text="non", command =question_8_n, value = 31)


    label.place(x=675,y=300)
    radiobutton30.place(x=700,y=320)
    radiobutton31.place(x=700,y=340)


def question_8_o():
    """Affecte aux variable de contage global 1 pour déterminer la catégorie la
    mieux adaptée, et ferme la fenêtre précèdente en ouvrant la suivante. La fonction
    s'active aux choix de la question de la fenêtre précèdente
    """
    # Appel de la fonction ouvrant la fenêtre suivante et destruction de la précèdente
    global newWindow8
    openlastWindow
    newWindow8.destroy()
    # Affectation de 1 au variables de contages globales
    global categorie_collectif
    categorie_collectif =categorie_collectif+1
    global categorie_plaisant
    categorie_plaisant = categorie_plaisant+1
    global categorie_combat
    categorie_combat = categorie_combat+1

def question_8_n():
    """Ferme la fenêtre précèdente en ouvrant la suivante. La fonction
    s'active aux choix de la question de la fenêtre précèdente
    """
    # Appel de la fonction ouvrant la fenêtre suivante et destruction de la précèdente
    global newWindow8
    openlastWindow()
    newWindow8.destroy()


def openlastWindow():
    """Affichage de la dernière page après le questionnaire, où apparait un bouton
    permettant de quitter la fenêtre ouverte et le bouton des fiches d'identités
    des sport allant au mieux pour l'utilisateur.
    """
    global categorie_collectif
    global categorie_raquette
    global categorie_plaisant
    global categorie_combat
    global categorie_eau
    global categorie_special

    # Initialisation d'un dictionnaire avec le nom des variables de contage globales en clé et leur valeur en valeur
    categorie = {"cat_col" : categorie_collectif, "cat_raq" : categorie_raquette, "cat_pla" : categorie_plaisant, "cat_com" : categorie_combat, "cat_eau" : categorie_eau, "cat_spe" : categorie_special}
    # Initialistion d'une liste qui contiendra les clés du dictionnaire avec la plus grande valeur
    sport_parfait = []
    # Initialisation de la variable contenant le max des valeurs déjà balayer
    max = 0

    # Balayage du dictionnaire par couple clé,valeur
    for k,v in categorie.items():
        #verifient si la valeur est supérieur au maximum
        if v > max :
            max = v
            sport_parfait = [k]
        elif v == max :
            sport_parfait.append(k)

    # création de la fenêtre final du questionnaire en définnissant les caractéristiques de la pâge (couleur de fond / teste)
    lastWindow = Toplevel(Mafenetre)
    lastWindow.title("")
    lastWindow.attributes('-fullscreen', True)
    lastWindow['bg']='#608892'
    # création d'un bouton qui permet de quitter/détruire la fenêtre
    Bouton_quitlast = Button(lastWindow, text = 'Quitter', command = lastWindow.destroy, bg = 'red', activebackground = 'black')
    Bouton_quitlast.place(x=700,y=800)


    #Vérification de la présence d'une catégorie dans la liste des sports avec la plus grande valeur et affiche le bouton permettant d'afficher la fiche d'identité des sports e la catégorie
    if "cat_col" in sport_parfait :
        Bouton_quitlast = Button(lastWindow, text = 'foot', command = fiche_foot , activebackground = 'black')
        Bouton_quitlast.place(x=350,y=600)
        Bouton_quitlast = Button(lastWindow, text = 'rugby', command = fiche_rugby , activebackground = 'black')
        Bouton_quitlast.place(x=350,y=550)

    if "cat_raq" in sport_parfait :
        Bouton_quitlast = Button(lastWindow, text = 'tennis', command = fiche_tennis , activebackground = 'black')
        Bouton_quitlast.place(x=450,y=600)
        Bouton_quitlast = Button(lastWindow, text = 'badminton', command = fiche_badminton , activebackground = 'black')
        Bouton_quitlast.place(x=450,y=550)

    if "cat_pla" in sport_parfait :
        Bouton_quitlast = Button(lastWindow, text = 'pétanque', command = fiche_petanque , activebackground = 'black')
        Bouton_quitlast.place(x=650,y=600)
        Bouton_quitlast = Button(lastWindow, text = 'golf', command = fiche_golf , activebackground = 'black')
        Bouton_quitlast.place(x=650,y=550)

    if "cat_com" in sport_parfait :
        Bouton_quitlast = Button(lastWindow, text = 'judo', command = fiche_judo , activebackground = 'black')
        Bouton_quitlast.place(x=750,y=600)
        Bouton_quitlast = Button(lastWindow, text = 'escrime', command = fiche_escrime , activebackground = 'black')
        Bouton_quitlast.place(x=750,y=550)

    if "cat_eau" in sport_parfait :
        Bouton_quitlast = Button(lastWindow, text = 'waterpolo', command = fiche_waterpolo , activebackground = 'black')
        Bouton_quitlast.place(x=850,y=600)
        Bouton_quitlast = Button(lastWindow, text = 'natation', command = fiche_natation , activebackground = 'black')
        Bouton_quitlast.place(x=850,y=550)

    if "cat_spe" in sport_parfait :
        Bouton_quitlast = Button(lastWindow, text = 'escalade', command = fiche_escalade , activebackground = 'black')
        Bouton_quitlast.place(x=950,y=600)
        Bouton_quitlast = Button(lastWindow, text = 'ski', command = fiche_ski , activebackground = 'black')
        Bouton_quitlast.place(x=950,y=550)


def fiche_foot():
    """Affiche la fiche d'identité du foot dans une nouvelle fenêtre, après avoir cliqué sur le bouton du
    nom du sport dans la fenêtre finale du projet.
    """
    f_foot = Toplevel()
    f_foot.title("Foot")

    # Charger une image
    image = Image.open(r"foot.png")
    resize_image = image.resize((1150, 700))

    # Convertir l'image pour l'affichage dans Tkinter
    photo = ImageTk.PhotoImage(resize_image)

    # Créer un widget Label pour afficher l'image
    label = Label(f_foot, image=photo)
    label.image = photo  # Conserver une référence à l'image pour éviter la suppression par le garbage collector

    # Ajouter le widget Label à la fenêtre
    label.pack()

def fiche_rugby():
    """Affiche la fiche d'identité du rugby dans une nouvelle fenêtre, après avoir cliqué sur le bouton du
    nom du sport dans la fenêtre finale du projet.
    """
    f_rugby = Toplevel()
    f_rugby.title("rugby")

    # Charger une image
    image = Image.open(r"rugby.png")
    resize_image = image.resize((450, 700))

    # Convertir l'image pour l'affichage dans Tkinter
    photo = ImageTk.PhotoImage(resize_image)

    # Créer un widget Label pour afficher l'image
    label = Label(f_rugby, image=photo)
    label.image = photo  # Conserver une référence à l'image pour éviter la suppression par le garbage collector

    # Ajouter le widget Label à la fenêtre
    label.pack()

def fiche_tennis():
    """Affiche la fiche d'identité du tennis dans une nouvelle fenêtre, après avoir cliqué sur le bouton du
    nom du sport dans la fenêtre finale du projet.
    """
    f_tennis = Toplevel()
    f_tennis.title("tennis")

    # Charger une image
    image = Image.open(r"tennis.png")
    resize_image = image.resize((1000, 700))

    # Convertir l'image pour l'affichage dans Tkinter
    photo = ImageTk.PhotoImage(resize_image)

    # Créer un widget Label pour afficher l'image
    label = Label(f_tennis, image=photo)
    label.image = photo  # Conserver une référence à l'image pour éviter la suppression par le garbage collector

    # Ajouter le widget Label à la fenêtre
    label.pack()

def fiche_badminton():
    """Affiche la fiche d'identité du badminton dans une nouvelle fenêtre, après avoir cliqué sur le bouton du
    nom du sport dans la fenêtre finale du projet.
    """
    f_badminton = Toplevel()
    f_badminton.title("badminton")

    # Charger une image
    image = Image.open(r"badminton.png")
    resize_image = image.resize((450, 700))

    # Convertir l'image pour l'affichage dans Tkinter
    photo = ImageTk.PhotoImage(resize_image)

    # Créer un widget Label pour afficher l'image
    label = Label(f_badminton, image=photo)
    label.image = photo  # Conserver une référence à l'image pour éviter la suppression par le garbage collector

    # Ajouter le widget Label à la fenêtre
    label.pack()

def fiche_petanque():
    """Affiche la fiche d'identité du pétanque dans une nouvelle fenêtre, après avoir cliqué sur le bouton du
    nom du sport dans la fenêtre finale du projet.
    """
    f_petanque = Toplevel()
    f_petanque.title("pétanque")

    # Charger une image
    image = Image.open(r"petanque.png")
    resize_image = image.resize((450, 700))

    # Convertir l'image pour l'affichage dans Tkinter
    photo = ImageTk.PhotoImage(resize_image)

    # Créer un widget Label pour afficher l'image
    label = Label(f_petanque, image=photo)
    label.image = photo  # Conserver une référence à l'image pour éviter la suppression par le garbage collector

    # Ajouter le widget Label à la fenêtre
    label.pack()

def fiche_golf():
    """Affiche la fiche d'identité du golf dans une nouvelle fenêtre, après avoir cliqué sur le bouton du
    nom du sport dans la fenêtre finale du projet.
    """
    f_golf = Toplevel()
    f_golf.title("golf")

    # Charger une image
    image = Image.open(r"golf.png")
    resize_image = image.resize((450, 700))

    # Convertir l'image pour l'affichage dans Tkinter
    photo = ImageTk.PhotoImage(resize_image)

    # Créer un widget Label pour afficher l'image
    label = Label(f_golf, image=photo)
    label.image = photo  # Conserver une référence à l'image pour éviter la suppression par le garbage collector

    # Ajouter le widget Label à la fenêtre
    label.pack()

def fiche_judo():
    """Affiche la fiche d'identité du judo dans une nouvelle fenêtre, après avoir cliqué sur le bouton du
    nom du sport dans la fenêtre finale du projet.
    """
    f_judo = Toplevel()
    f_judo.title("judo")

    # Charger une image
    image = Image.open(r"judo.png")
    resize_image = image.resize((1000, 700))

    # Convertir l'image pour l'affichage dans Tkinter
    photo = ImageTk.PhotoImage(resize_image)

    # Créer un widget Label pour afficher l'image
    label = Label(f_judo, image=photo)
    label.image = photo  # Conserver une référence à l'image pour éviter la suppression par le garbage collector

    # Ajouter le widget Label à la fenêtre
    label.pack()

def fiche_escrime():
    """Affiche la fiche d'identité du escrime dans une nouvelle fenêtre, après avoir cliqué sur le bouton du
    nom du sport dans la fenêtre finale du projet.
    """
    f_escrime = Toplevel()
    f_escrime.title("escrime")

    # Charger une image
    image = Image.open(r"escrime.png")
    resize_image = image.resize((450, 700))

    # Convertir l'image pour l'affichage dans Tkinter
    photo = ImageTk.PhotoImage(resize_image)

    # Créer un widget Label pour afficher l'image
    label = Label(f_escrime, image=photo)
    label.image = photo  # Conserver une référence à l'image pour éviter la suppression par le garbage collector

    # Ajouter le widget Label à la fenêtre
    label.pack()

def fiche_waterpolo():
    """Affiche la fiche d'identité du waterpolo dans une nouvelle fenêtre, après avoir cliqué sur le bouton du
    nom du sport dans la fenêtre finale du projet.
    """
    f_waterpolo = Toplevel()
    f_waterpolo.title("waterpolo")

    # Charger une image
    image = Image.open(r"waterpolo.png")
    resize_image = image.resize((450, 700))

    # Convertir l'image pour l'affichage dans Tkinter
    photo = ImageTk.PhotoImage(resize_image)

    # Créer un widget Label pour afficher l'image
    label = Label(f_waterpolo, image=photo)
    label.image = photo  # Conserver une référence à l'image pour éviter la suppression par le garbage collector

    # Ajouter le widget Label à la fenêtre
    label.pack()

def fiche_natation():
    """Affiche la fiche d'identité du natation dans une nouvelle fenêtre, après avoir cliqué sur le bouton du
    nom du sport dans la fenêtre finale du projet.
    """
    f_natation = Toplevel()
    f_natation.title("natation")

    # Charger une image
    image = Image.open(r"natation.png")
    resize_image = image.resize((450, 700))

    # Convertir l'image pour l'affichage dans Tkinter
    photo = ImageTk.PhotoImage(resize_image)

    # Créer un widget Label pour afficher l'image
    label = Label(f_natation, image=photo)
    label.image = photo  # Conserver une référence à l'image pour éviter la suppression par le garbage collector

    # Ajouter le widget Label à la fenêtre
    label.pack()

def fiche_escalade():
    """Affiche la fiche d'identité du escalade dans une nouvelle fenêtre, après avoir cliqué sur le bouton du
    nom du sport dans la fenêtre finale du projet.
    """
    f_escalade = Toplevel()
    f_escalade.title("escalade")

    # Charger une image
    image = Image.open(r"escalade.png")
    resize_image = image.resize((450, 700))

    # Convertir l'image pour l'affichage dans Tkinter
    photo = ImageTk.PhotoImage(resize_image)

    # Créer un widget Label pour afficher l'image
    label = Label(f_escalade, image=photo)
    label.image = photo  # Conserver une référence à l'image pour éviter la suppression par le garbage collector

    # Ajouter le widget Label à la fenêtre
    label.pack()

def fiche_ski():
    """Affiche la fiche d'identité du ski dans une nouvelle fenêtre, après avoir cliqué sur le bouton du
    nom du sport dans la fenêtre finale du projet.
    """
    f_ski = Toplevel()
    f_ski.title("ski")

    # Charger une image
    image = Image.open(r"ski.png")
    resize_image = image.resize((800, 700))

    # Convertir l'image pour l'affichage dans Tkinter
    photo = ImageTk.PhotoImage(resize_image)

    # Créer un widget Label pour afficher l'image
    label = Label(f_ski, image=photo)
    label.image = photo  # Conserver une référence à l'image pour éviter la suppression par le garbage collector

    # Ajouter le widget Label à la fenêtre
    label.pack()





# Création de la fenêtre principale
Mafenetre = Tk()
Mafenetre.attributes('-fullscreen', True)
Mafenetre.title("page d'acceuille du quizz")
Mafenetre['bg']='#0A4E4E' # couleur de fond


# Charger une image
image = Image.open(r"tindsport.png")
resize_image = image.resize((250, 250))

# Convertir l'image pour l'affichage dans Tkinter
photo = ImageTk.PhotoImage(resize_image)

# Créer un widget Label pour afficher l'image
label = Label(Mafenetre, image=photo)
label.image = photo  # Conserver une référence à l'image pour éviter la suppression par le garbage collector

# Ajouter le widget Label à la fenêtre
label.place(x=1300,y=25)


# Création d'un widget Label
Label1 = Label(Mafenetre, text = 'TIND SPORT', fg = 'white', bg = '#0A4E4E')
Label1.config(font=("Impact",70))
# Positionnement du widget avec la méthode pack()
Label1.pack(padx = 10 , pady = 10)


# Création d'un widget Label
Label3 = Label(Mafenetre, text = 'le tinder du sport', fg = 'white', bg = '#0A4E4E')
Label3.config(font=("Georgia",30))
Label3.pack(padx = 10 , pady = 10)

# A ffichage du bouton permettant de lancer le quizz et donc d'appeler la première fonction avec la première question du quizz
Bouton0 = Button(Mafenetre, text = 'commencer le quizz',command = opennewWindow , bg = '#0A4E4E',fg = 'white', activebackground = 'black')
Bouton0.config(font=("Courier",50))
Bouton0.pack(padx = 5 , pady = 5)

# Création d'un widget Button (bouton Quitter)
Bouton1 = Button(Mafenetre, text = 'Quitter', command = Mafenetre.destroy, bg = 'red', activebackground = 'black')
Bouton1.pack(side=BOTTOM,padx=20,pady=20)



# Charger une image
image3 = Image.open(r"logo-jeu-olympique.png")
resize_image3 = image3.resize((250,250))

# Convertir l'image pour l'affichage dans Tkinter
photo3 = ImageTk.PhotoImage(resize_image3)

# Créer un widget Label pour afficher l'image
label3 = Label(Mafenetre, image=photo3)
label3.image = photo3  # Conserver une référence à l'image pour éviter la suppression par le garbage collector

# Ajouter le widget Label à la fenêtre
label3.place(x=25,y=25)


# Lancement du gestionnaire d'événements
Mafenetre.mainloop()

