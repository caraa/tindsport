TINDSPORT
Le Tinder du sport

1.	Ouvrir le programme sur python ou Edupython.
2.	Exécuter le programme, une interface va s’afficher.
3.	Cliquer sur le bouton « Commencer le quiz ».
4.	Répondre aux questions afficher puis appuyer sur le bouton texte pour passer à la question suivante. Attention à ne pas se tromper de réponse, il est impossible de revenir à la question précédente.
5.	Une page finale s’affichera avec le résultat.
6.	Cliquer sur le bouton avec le sport qui vous convient pour avoir des informations sur le sport, une fiche descriptive du sport s’affichera 
7.	Pour quitter le quiz cliquer sur le bouton quitter, celui-ci fermera toutes les interfaces ouvertes.
